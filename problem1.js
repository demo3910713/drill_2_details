//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function allWebDevelopers(data) {
  // count the webDevelopers
  let count = 0;
  if (Array.isArray(data)) {
    //store the Details
    let info = data.filter(value => {
      if(value.job.includes("Web Developer"))
      {
        return value
      }
    })
    let count = info.length
    return {info , count }
  } else {
    return " Data Insufficient";
  }
}
// exports the code
module.exports = { allWebDevelopers };
