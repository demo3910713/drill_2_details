// Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something.

function eachSalaryAmountIsFactorOf10000(data) {
  if (Array.isArray(data)) {
    let salaries_In_Numbers = data.map((value) =>
    {
      return Math.round(Number(value.salary.replace('$',''))*10000);
    })
  return salaries_In_Numbers
  } else {
    return "Data Unsufficeint";
  }
}
// exports the code
module.exports = { eachSalaryAmountIsFactorOf10000 };
