//4. Find the sum of all salaries.
function sumOfAllSalaries(data) {
  if (Array.isArray(data)) {
    let allSalaries = data.reduce((acc, value) =>
    {
      const salaryInString = value.salary.replace("$", "");
      // convertion String to  Number
     const salaryInNumber = Number(salaryInString);
      return acc+salaryInNumber
    },0)
    return allSalaries;
  } else {
    return "Data Unsufficeint";
  }
}
// export the code
module.exports = { sumOfAllSalaries };
