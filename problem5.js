//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).
let details = module.require("./data");
let data = details.details;
function sumOfAllSalariesBasedOnCountry(data) {
  if (Array.isArray(data)) {
    // store the sum of salaries by country wise
    let allSalariesByCountry = {};
    data.forEach(element => {
      const country = element.location
      //convert string to Number
      const salary = Number(element.salary.replace("$", ""));
      // insert into the allSalariesByCountry
        if (!allSalariesByCountry[country]) {
         allSalariesByCountry[country] = 0;
       }
       allSalariesByCountry[country] += salary;  
    });
    return allSalariesByCountry;
  } else {
    return "Data Insufficient ";
  }
}
// export the code
module.exports = { sumOfAllSalariesBasedOnCountry}
