
// Convert all the salary values into proper numbers instead of strings.
function allSalariesStringIntoNumber(data) {
    if(Array.isArray(data)) {
      let salaries_In_Numbers = data.map((value) =>
        {
          return Number(value.salary.replace('$',''));
        })
      return salaries_In_Numbers
    }
    else
    {
        return("Data Unsufficeint")
    }
    
}
// Exports the Data
module.exports = { allSalariesStringIntoNumber}
