//6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).
function averagefAllSalariesBasedOnCountry(data) {
  if (Array.isArray(data)) {
    // summation by country wise
    let allSalariesByCountry = {};
    let countOfCountry = {};
    let allAverageSalariesByCountry ={}
    data.forEach((element) => {
      const country = element.location;
      //convert string to Number
      const salary = Number(element.salary.replace("$", ""));
      // insert into the allSalariesByCountry
      if (!allSalariesByCountry[country]) {
        allSalariesByCountry[country] = 0;
        countOfCountry[country] = 0;
      }
      allSalariesByCountry[country] += salary;
      countOfCountry[country]++;
    });
    for (const key in allSalariesByCountry) {
      allAverageSalariesByCountry[key] = allSalariesByCountry[key]/ countOfCountry[key];
    }
    return allAverageSalariesByCountry
  } else {
    return "Data Insufficient ";
  }
}
// export the code
module.exports = { averagefAllSalariesBasedOnCountry };
